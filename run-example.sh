#!/usr/bin/env bash

rm -rf example/test-set-1/result/*
java -jar out/artifacts/DirectoryMerge_jar/DirectoryMerge.jar    \
            example/test-set-1/reference-old                        \
            example/test-set-1/reference-new                        \
            example/test-set-1/local                                \
            example/test-set-1/result                               \

open example/test-set-1/result


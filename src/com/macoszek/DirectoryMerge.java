package com.macoszek;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import name.fraser.neil.plaintext.diff_match_patch;

public class DirectoryMerge
{
    diff_match_patch diffMatchPatchService = new diff_match_patch();

    public static void main(String[] args) throws IOException
    {
        if (args.length != 4) {
            System.out.println("Usage: DirectoryMerge [referenceOldDirectory] [referenceNewDirectory] [localChangesDirectory] [mergeResultDirectory]");
        }
        File currentDirectory = new File(new File(".").getAbsolutePath());
        String basePath = currentDirectory.getCanonicalPath();
        String referenceOldDirectory = basePath + File.separator + args[0];
        String referenceNewDirectory = basePath + File.separator + args[1];
        String localChangesDirectory = basePath + File.separator + args[2];
        String mergeResultPath = basePath + File.separator + args[3];

        new DirectoryMerge(referenceOldDirectory, referenceNewDirectory, localChangesDirectory, mergeResultPath);
    }

    public DirectoryMerge(String referenceOldDirectory, String referenceNewDirectory, String localChangesDirectory, String mergeResultPath) throws IOException
    {
        List<Item> directoryMergeResult = mergeDirectories(referenceOldDirectory, referenceNewDirectory, localChangesDirectory);
        storeMergeResult(directoryMergeResult, mergeResultPath);
    }

    /**************************************************************************************************
     * Actions
     **************************************************************************************************/

    protected List<Item> mergeDirectories(String referenceOldDirectory, String referenceNewDirectory, String localChangesDirectory) throws IOException
    {
        List<Item> classifiedItems = new ArrayList<>();

        Set<String> localFiles = readFileNamesInDirectory(localChangesDirectory);
        Set<String> localSubdirectories = readSubdirectoryNamesInDirectory(localChangesDirectory);
        Set<String> oldFiles = readFileNamesInDirectory(referenceOldDirectory);
        Set<String> oldSubdirectories = readSubdirectoryNamesInDirectory(referenceOldDirectory);
        Set<String> newFiles = readFileNamesInDirectory(referenceNewDirectory);
        Set<String> newSubdirectories = readSubdirectoryNamesInDirectory(referenceNewDirectory);

        // determine deleted old files and modified old files
        for (String oldFile : oldFiles) {
            if (newFiles.contains(oldFile)) {
                if (localFiles.contains(oldFile)) {
                    Item item = new FileItem(oldFile, State.NOT_MODIFIED);
                    item.mergeResult = mergeFiles(
                        referenceOldDirectory + File.separator + oldFile,
                        referenceNewDirectory + File.separator + oldFile,
                        localChangesDirectory + File.separator + oldFile
                    );
                    if (item.mergeResult.isModified) {
                        item.state = State.MODIFIED_FILE_CONTENT;
                    }
                    if (!item.mergeResult.conflicts.isEmpty()) {
                        item.state = State.CONFLICT;
                    }
                    classifiedItems.add(item);
                } else {
                    Item item = new FileItem(oldFile, State.CONFLICT);
                    item.mergeResult.conflicts.add("Local version was deleted, although is present in old and new versions");
                    classifiedItems.add(item);
                }
            } else if (newSubdirectories.contains(oldFile)) {
                Item item = new FileItem(oldFile, State.TRANSFORMED_FILE_INTO_NEW_DIRECTORY);
                if (localFiles.contains(oldFile)) {
                    item.mergeResult = mergeFiles(
                        referenceOldDirectory + File.separator + oldFile,
                        localChangesDirectory + File.separator + oldFile,
                        localChangesDirectory + File.separator + oldFile
                    );
                    if (item.mergeResult.isModified) {
                        item.state = State.CONFLICT;
                        item.mergeResult.conflicts.add("Local version was modified, although is present in old version should be removed and replaced by directory");
                    }
                }
                classifiedItems.add(item);
            } else {
                Item item = new FileItem(oldFile, State.DELETED);
                if (localFiles.contains(oldFile)) {
                    item.mergeResult = mergeFiles(
                        referenceOldDirectory + File.separator + oldFile,
                        localChangesDirectory + File.separator + oldFile,
                        localChangesDirectory + File.separator + oldFile
                    );
                    if (item.mergeResult.isModified) {
                        item.state = State.CONFLICT;
                        item.mergeResult.conflicts.add("Local version was modified, although old version should be removed");
                    }
                }
                classifiedItems.add(item);
            }
        }

        // determine new added files
        for (String newFile : newFiles) {
            if (oldFiles.contains(newFile)) {
                // already solved
            } else if (oldSubdirectories.contains(newFile)) {
                Item item = new DirectoryItem(newFile, State.TRANSFORMED_DIRECTORY_INTO_NEW_FILE);
                if (localSubdirectories.contains(newFile)) {
                    item.directoryMergeResult = mergeDirectories(
                        referenceOldDirectory + File.separator + newFile,
                        localChangesDirectory + File.separator + newFile,
                        localChangesDirectory + File.separator + newFile
                    );
                    for (Item subitem : item.directoryMergeResult) {
                        if (subitem.state != State.NOT_MODIFIED) {
                            item.state = State.CONFLICT;
                            item.mergeResult.conflicts.add("Local version of directory was modified, although is old version should be removed and replaced by file");
                            break;
                        }
                    }
                }
                classifiedItems.add(item);
            } else {
                Item item = new FileItem(newFile, State.NEW);
                if (localFiles.contains(newFile)) {
                    item.state = State.CONFLICT;
                    item.mergeResult.conflicts.add("Local version contains file that in old does not exist, and this file should be overridden with file with same name from new version");
                } else {
                    item.mergeResult.mergedContent = loadFileContent(referenceNewDirectory + File.separator + newFile);
                }
                classifiedItems.add(item);
            }
        }

        // determine deleted old directories and modified directories
        for (String oldDirectory : oldSubdirectories) {
            if (newSubdirectories.contains(oldDirectory)) {
                Item item = new DirectoryItem(oldDirectory, State.NOT_MODIFIED);
                if (localSubdirectories.contains(oldDirectory)) {
                    item.directoryMergeResult = mergeDirectories(
                            referenceOldDirectory + File.separator + oldDirectory,
                            referenceNewDirectory + File.separator + oldDirectory,
                            localChangesDirectory + File.separator + oldDirectory
                    );
                    for (Item classifiedItem : item.directoryMergeResult) {
                        if (classifiedItem.state != State.NOT_MODIFIED) {
                            item.state = State.MODIFIED_DIRECTORY_CONTENT;
                        }
                    }
                } else {
                    item.state = State.CONFLICT;
                    item.mergeResult.conflicts.add("Local version of directory should exists, but was removed.");
                }
                classifiedItems.add(item);
            } else {
                classifiedItems.add(new DirectoryItem(oldDirectory, State.DELETED));
            }
        }

        // determine new directories
        for (String newDirectory : newSubdirectories) {
            if (oldSubdirectories.contains(newDirectory)) {
                // already solved
            } else {
                Item item = new DirectoryItem(newDirectory, State.NEW);
                if (localSubdirectories.contains(newDirectory)) {
                    item.state = State.CONFLICT;
                    item.mergeResult.conflicts.add("Local version contains directory that in old does not exist, and this directory should be overridden with directory with same name from new version.");
                }
                classifiedItems.add(item);
            }
        }

        return classifiedItems;
    }

    protected MergeResult mergeFiles(String oldFile, String newFile, String localFile) throws IOException
    {
        MergeResult mergeResult = new MergeResult();
        String oldFileContent = loadFileContent(oldFile);
        String newFileContent = loadFileContent(newFile);
        String localFileContent = loadFileContent(localFile);

        LinkedList<diff_match_patch.Patch> patches = diffMatchPatchService.patch_make(oldFileContent, newFileContent);
        Object[] patchedResult = diffMatchPatchService.patch_apply(patches, localFileContent);

        if (oldFileContent.equals(newFileContent)) {
            mergeResult.isModified = false;
        } else {
            mergeResult.isModified = true;
        }
        mergeResult.mergedContent = ((String) patchedResult[0]);

        boolean[] patchSuccessResult = (boolean[])patchedResult[1];
        int i = 0;
        for (diff_match_patch.Patch patch : patches) {
            mergeResult.patches.add(patch.toString());
            if (!patchSuccessResult[i]) {
                mergeResult.conflicts.add(patch.toString());
            }
            i++;
        }


        return mergeResult;
    }

    protected void storeMergeResult(List<Item> directoryMergeResult, String mergeResultPath) throws IOException
    {
        System.out.println("Writing into " + mergeResultPath + " \n");
        for (Item item : directoryMergeResult) {
            switch (item.state) {
                case NEW:
                case NOT_MODIFIED:
                case MODIFIED_FILE_CONTENT:
                    {
                        System.out.println("Writing " + item.toString());
                        if (item.type == ItemType.FILE) {
                            FileWriter fw = new FileWriter(mergeResultPath + File.separator + item.name);
                            fw.write(item.mergeResult.mergedContent);
                            fw.close();;
                        } else {
                            Files.createDirectory(Paths.get(mergeResultPath + File.separator + item.name));
                        }
                    }
                    break;

                case CONFLICT:
                    {
                        System.out.println("Conflicting " + item.toString());
                        if (item.type == ItemType.FILE) {
                            FileWriter fw = new FileWriter(mergeResultPath + File.separator + item.name);
                            fw.write(item.mergeResult.mergedContent);
                            fw.close();;
                        } else {
                            Files.createDirectory(Paths.get(mergeResultPath + File.separator + item.name));
                        }
                    }
                    break;

                case DELETED:
                case MODIFIED_DIRECTORY_CONTENT:
                case TRANSFORMED_DIRECTORY_INTO_NEW_FILE:
                case TRANSFORMED_FILE_INTO_NEW_DIRECTORY:
                    System.out.println("Skipping " + item.toString());
                    break;
            }


        }
    }

    /**************************************************************************************************
     * Helper methods
     **************************************************************************************************/

    public Set<String> readFileNamesInDirectory(String directory)
    {
        Set<String> names = new HashSet<>();

        try {
            Path directoryPath = Paths.get(directory);
            Files.walk(directoryPath).forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    names.add(filePath.getFileName().toString());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return names;
    }

    public Set<String> readSubdirectoryNamesInDirectory(String directory)
    {
        Set<String> names = new HashSet<>();

        try {
            Path directoryPath = Paths.get(directory);
            Files.walk(directoryPath).forEach(filePath -> {
                try {
                    if (Files.isDirectory(filePath) && !Files.isSameFile(filePath, directoryPath)) {
                        names.add(filePath.getFileName().toString());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return names;
    }

    // source: http://stackoverflow.com/questions/326390/how-to-create-a-java-string-from-the-contents-of-a-file
    public String loadFileContent(String filePath) throws IOException
    {
        byte[] encoded = new byte[0];
        encoded = Files.readAllBytes(Paths.get(filePath));
        return new String(encoded, StandardCharsets.UTF_8);
    }

    /**************************************************************************************************
     * Helper internal structures
     **************************************************************************************************/

    private enum State {
        NEW,
        DELETED, NOT_MODIFIED,
        MODIFIED_FILE_CONTENT, MODIFIED_DIRECTORY_CONTENT,
        CONFLICT,
        TRANSFORMED_DIRECTORY_INTO_NEW_FILE, TRANSFORMED_FILE_INTO_NEW_DIRECTORY
    }

    private enum ItemType {
        DIRECTORY, FILE
    }

    private class Item
    {
        public String name;
        public State state;
        public ItemType type;
        public List<Item> directoryMergeResult = new ArrayList<>();
        public MergeResult mergeResult = new MergeResult();

        public Item(String name, State state, ItemType type) {
            this.name = name;
            this.state = state;
            this.type = type;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append(name);
            sb.append("] - state: ");
            sb.append(state.name());
            sb.append(" - type: ");
            sb.append(type.name());
            sb.append("\n");
            if (mergeResult.conflicts.size() > 0) {
                sb.append("conflicts:\n");
                for (String conflict : mergeResult.conflicts) {
                    sb.append("").append(conflict).append("\n");
                }
            }
            if (mergeResult.patches.size() > 0) {
                sb.append("patches:\n");
                for (String patch : mergeResult.patches) {
                    sb.append("").append(patch).append("\n");
                }
            }
            sb.append("------------------------------------------------------------------\n");
            return sb.toString();
        }
    }

    private class DirectoryItem extends Item
    {
        public DirectoryItem(String name, State state) {
            super(name, state, ItemType.DIRECTORY);
        }
    }

    private class FileItem extends Item
    {
        public FileItem(String name, State state) {
            super(name, state, ItemType.FILE);
        }
    }

    private class MergeResult
    {
        public boolean isModified = false;
        public String mergedContent;
        public List<String> conflicts = new ArrayList<>();
        public List<String> patches = new ArrayList<>();

        public MergeResult() {}
    }
}

# Ukázka použití

    otevřete Terminal nebo konzoli
    chmod 770 run.sh
    ./run.sh

# Princip fungování 
1. určí které soubory jsou ke smazání
2. určí které soubory byly aktualizovány a ty se pokusí mergovat (případné neúspěchy si pamatuje s příznakem CONFLICT)
3. určí které soubory jsou nové
4. určí které adresáře jsou ke smazání
5. určí které adresáře byly aktualizovány a ty se pokusí mergovat (případné neúspěchy si pamatuje s příznakem CONFLICT)
6. určí které adresáře jsou nové

Pro mergování textu používám knihovnu diff_match_patch
https://neil.fraser.name/software/diff_match_patch/svn/trunk/demos/demo_patch.html

# Todo

* storeMergeResult aby vypisoval XML
* storeMergeResult aby vypisoval také vnořené adresáře